from flask import Flask, request, render_template, redirect, url_for, jsonify
from celery import Celery, shared_task, Task
import boto3


ACCESS_KEY = 'ACESS_KEY_AWS'
SECRET_KEY = 'SECRET_KEY_AWS'
QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/123456789/nome_fila' 

# busca da sessao senao ele ira pegar da pasta .aws por default
session = boto3.Session(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    region_name='us-east-1'
)

sqs = session.client('sqs')

def celery_init_app(app: Flask):
    class FlaskTask(Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_app = Celery(app.name)
    celery_app.config_from_object(app.config["CELERY"])
    celery_app.Task = FlaskTask
    celery_app.set_default()
    app.extensions["celery"] = celery_app
    return celery_app

app = Flask(__name__)

app.config.from_mapping(
    CELERY=dict(
        broker_url=f'sqs://{ACCESS_KEY}:{SECRET_KEY}@',
        task_ignore_result=True,
        result_backend='db+sqlite:///celery.sqlite',
            beat_schedule={
                "task-every-10-seconds": {
                    "task": "app.process_task",
                    "schedule": 10,
                }
            }
    ),
)
celery_app = celery_init_app(app)


@shared_task(ignore_result=False)
def process_task():
    # Recebe mensagens da fila SQS
    response = sqs.receive_message(
        QueueUrl=QUEUE_URL,
        MaxNumberOfMessages=5,
        VisibilityTimeout=10,
        WaitTimeSeconds=10  
    )

    messages = response.get('Messages', [])
    
    for message in messages:
        try:
            # Processa a mensagem
            data = message['Body']
            print("Processing:", data)
            
            # Após processar, exclui a mensagem da fila SQS
            receipt_handle = message['ReceiptHandle']
            sqs.delete_message(
                QueueUrl=QUEUE_URL,
                ReceiptHandle=receipt_handle
            )
            print("Message deleted:", message['MessageId'])
        except Exception as e:
            print("Error processing message")


@app.route("/",methods=["GET","POST"])
def index():
    if request.method == 'GET':
        return """<h1>
        <form method="post">
            <label>Message</label>
            <input name="message"/>
            <button type="submit">OK</button>
        </form>
        </h1>"""
    if request.method == 'POST':
        message = request.form.get('message')

        if message:

            try:
                response = sqs.send_message(
                    QueueUrl=QUEUE_URL,
                    MessageBody=message
                )
                print("Message sent:", response)
            except Exception as e:
                print("Error sending message:", str(e))
                raise e

        return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)